const topIconsCards = document.querySelectorAll(".topIconCards");
const topIconsButtons = document.querySelectorAll(".topIcon");
const chatNow = document.querySelector(".chat img");
const chatNowCard = document.querySelector(".chatNow");
const advisorChat = document.querySelector(".advisorChat");
const advisorProfileCard = document.querySelector(".advisorProfile");
const openChat = document.querySelector(".openChat");
const chatting = document.querySelector(".chatting");
const maximizebutton = document.querySelector("#maximization");
const maximizedCard = document.querySelector(".maximizeChat");
const closeCard = document.getElementsByClassName("close");
const buttonVault = document.querySelector(".vault");
const cashflowCard = document.querySelector(".card-cashflow");
const editButton = document.querySelector("#edit");
const reportButton = document.querySelector("#openReport");
const reportCard = document.querySelector(".card.report");
const settingInput = document.querySelector("#inputChanging");
const openDisplayBtn = document.querySelector("#openDisplaySettings");
const openSecurityBtn = document.querySelector("#openSecurity");
const openGrowthAssumption = document.getElementById("growth");
const openGrowthCard = document.querySelector(".card.assumbtion");
const securityCard = document.querySelector(".settingsSecurity"); //<==
const displayCard = document.querySelector(".settingsDisplay"); //<==
const settingCard = document.querySelector(".settingInput"); //<==
const editScreen = document.querySelector(".settingsProfile"); //<==
function openCloseTopTabs() {
  for (let i = 0; i < topIconsButtons.length; i++) {
    topIconsButtons[i].addEventListener("click", (e) => {
      if (topIconsCards[parseInt(e.target.name)].classList.contains("hide")) {
        topIconsCards[parseInt(e.target.name)].classList.remove("hide");
        topIconsCards[parseInt(e.target.name)].classList.toggle("activeCard");
      } else {
        topIconsCards[parseInt(e.target.name)].classList.add("hide");
      }
    });
  }
}

function cardAnimation(button, card) {
  button.addEventListener("click", () => {
    card.classList.toggle("hide");
  });
}
for (let i = 0; i < closeCard.length; i++) {
  closeCard[i].addEventListener("click", (e) => {
    let firstTarget = e.target.parentNode;
    let target = e.target.parentNode.parentNode;
    let secondTarget = e.target.parentNode.parentNode.parentNode;
    let thirdTarget = e.target.parentNode.parentNode.parentNode.parentNode;

    if (target.classList.contains("closable")) {
      target.classList.add("hide");
    }
    if (firstTarget.classList.contains("closable")) {
      firstTarget.classList.add("hide");
    }
    if (secondTarget.classList.contains("closable")) {
      secondTarget.classList.add("hide");
    }
    if (thirdTarget.classList.contains("closable")) {
      thirdTarget.classList.add("hide");
    }
  });
}
settingInput.addEventListener("click", () => {
  editScreen.classList.add("none");
  settingCard.classList.remove("none");
});

openCloseTopTabs();
cardAnimation(chatNow, chatNowCard);
cardAnimation(advisorChat, advisorProfileCard);
cardAnimation(openChat, chatting);
cardAnimation(maximizebutton, maximizedCard);
changeSetting(editButton, editScreen);
changeSetting(reportButton, reportCard);
changeSetting(settingInput, settingCard);
changeSetting(openDisplayBtn, displayCard);
changeSetting(openSecurityBtn, securityCard);
changeSetting(openGrowthAssumption, openGrowthCard);

const investButton = document.querySelector("#investing");

investButton.addEventListener("click", () => {
  cashflowCard.classList.add("none");
  investCard.classList.remove("none");
});

const rEstateButton = document.querySelector("#real-estate");

rEstateButton.addEventListener("click", () => {
  investCard.classList.add("none");
  rEstateCard.classList.remove("none");
});

const insuranceButton = document.querySelector("#insure");

insuranceButton.addEventListener("click", () => {
  rEstateCard.classList.add("none");
  insuranceCard.classList.remove("none");
});

const legalButton = document.querySelector("#legala");

legalButton.addEventListener("click", () => {
  insuranceCard.classList.add("none");
  legalCard.classList.remove("none");
});

const taxButton = document.querySelector("#taxPl");
taxButton.addEventListener("click", () => {
  legalCard.classList.add("none");
  taxCard.classList.remove("none");
});

function changeSetting(button, element) {
  button.addEventListener("click", () => {
    element.classList.remove("none");
  });
}

function changeSettingNavigation(button, element) {
  button.addEventListener("click", () => {
    hidePages();
    element.classList.remove("none");
  });
}

function hidePages() {
  const settingsPages = document.querySelectorAll(".set");
  for (let i = 0; i < settingsPages.length; i++) {
    settingsPages[i].classList.add("none");
  }
}
// Settings profile navigation

const buttonProfile = document.querySelector(" .changeToProfile");
const buttonInputs = document.querySelector(".changeToInputs");
const buttonDisplay = document.querySelector(" .changeToDisplay");
const buttonSecurity = document.querySelector(".changeToSecurity");

changeSettingNavigation(buttonProfile, editScreen);
changeSettingNavigation(buttonInputs, settingCard);
changeSettingNavigation(buttonDisplay, displayCard);
changeSettingNavigation(buttonSecurity, securityCard);

// Settings input navigation
const buttonProfile2 = document.querySelector(" .changeToProfile2");
const buttonInputs2 = document.querySelector(".changeToInputs2");
const buttonDisplay2 = document.querySelector(" .changeToDisplay2");
const buttonSecurity2 = document.querySelector(".changeToSecurity2");

changeSettingNavigation(buttonProfile2, editScreen);
changeSettingNavigation(buttonInputs2, settingCard);
changeSettingNavigation(buttonDisplay2, displayCard);
changeSettingNavigation(buttonSecurity2, securityCard);

// Settings display navigation
const buttonProfile3 = document.querySelector(" .changeToProfile3");
const buttonInputs3 = document.querySelector(".changeToInputs3");
const buttonDisplay3 = document.querySelector(" .changeToDisplay3");
const buttonSecurity3 = document.querySelector(".changeToSecurity3");

changeSettingNavigation(buttonProfile3, editScreen);
changeSettingNavigation(buttonInputs3, settingCard);
changeSettingNavigation(buttonDisplay3, displayCard);
changeSettingNavigation(buttonSecurity3, securityCard);

//Settings security navigation

const buttonProfile4 = document.querySelector(" .changeToProfile4");
const buttonInputs4 = document.querySelector(".changeToInputs4");
const buttonDisplay4 = document.querySelector(" .changeToDisplay4");
const buttonSecurity4 = document.querySelector(".changeToSecurity4");

changeSettingNavigation(buttonProfile4, editScreen);
changeSettingNavigation(buttonInputs4, settingCard);
changeSettingNavigation(buttonDisplay4, displayCard);
changeSettingNavigation(buttonSecurity4, securityCard);

function hideCards() {
  const cardChange = document.querySelectorAll(".cardChange");
  for (let i = 0; i < cardChange.length; i++) {
    cardChange[i].classList.add("none");
    cardChange[i].classList.add("hide");
  }
}

function changeCardNavigation(button, element) {
  button.addEventListener("click", () => {
    hideCards();
    element.classList.remove("none");
    element.classList.remove("hide");
  });
}
const cashCard = document.querySelector(".card-cashflow");
const taxCard = document.querySelector(".taxplan");
const legalCard = document.querySelector(".planning");
const insuranceCard = document.querySelector(".insurance");
const rEstateCard = document.querySelector(".realEstate");
const investCard = document.querySelector(".card.investments");

// Cash flow navigation

const cashButton123 = document.querySelector(".cashButton");
const investmentButton123 = document.querySelector(".investButton");
const realEstateButton123 = document.querySelector(".realEstateButton");
const insuranceButton123 = document.querySelector(".insuranceButton");
const legalPlanningButton123 = document.querySelector(".legalPlanningButton");
const taxPlaningButton123 = document.querySelector(".taxPlaningButton");

changeCardNavigation(investmentButton123, investCard);
changeCardNavigation(realEstateButton123, rEstateCard);
changeCardNavigation(insuranceButton123, insuranceCard);
changeCardNavigation(legalPlanningButton123, legalCard);
changeCardNavigation(taxPlaningButton123, taxCard);

// Investment navigation

const cashButton1 = document.querySelector(".cashButton1");
const investmentButton1 = document.querySelector(".investButton1");
const realEstateButton1 = document.querySelector(".realEstateButton1");
const insuranceButton1 = document.querySelector(".insuranceButton1");
const legalPlanningButton1 = document.querySelector(".legalPlanningButton1");
const taxPlaningButton1 = document.querySelector(".taxPlaningButton1");

changeCardNavigation(cashButton1, cashCard);
changeCardNavigation(realEstateButton1, rEstateCard);
changeCardNavigation(insuranceButton1, insuranceCard);
changeCardNavigation(legalPlanningButton1, legalCard);
changeCardNavigation(taxPlaningButton1, taxCard);

// Real estate navigation

const cashButton2 = document.querySelector(".cashButton2");
const investmentButton2 = document.querySelector(".investButton2");
const realEstateButton2 = document.querySelector(".realEstateButton2");
const insuranceButton2 = document.querySelector(".insuranceButton2");
const legalPlanningButton2 = document.querySelector(".legalPlanningButton2");
const taxPlaningButton2 = document.querySelector(".taxPlaningButton2");

changeCardNavigation(cashButton2, cashCard);
changeCardNavigation(investmentButton2, investCard);
changeCardNavigation(insuranceButton2, insuranceCard);
changeCardNavigation(legalPlanningButton2, legalCard);
changeCardNavigation(taxPlaningButton2, taxCard);

// Insurance navigation 

const cashButton3 = document.querySelector(".cashButton3");
const investmentButton3 = document.querySelector(".investButton3");
const realEstateButton3 = document.querySelector(".realEstateButton3");
const insuranceButton3 = document.querySelector(".insuranceButton3");
const legalPlanningButton3 = document.querySelector(".legalPlanningButton3");
const taxPlaningButton3 = document.querySelector(".taxPlaningButton3");

changeCardNavigation(cashButton3, cashCard);
changeCardNavigation(investmentButton3, investCard);
changeCardNavigation(realEstateButton3, rEstateCard);
changeCardNavigation(legalPlanningButton3, legalCard);
changeCardNavigation(taxPlaningButton3, taxCard);

// Legal planning navigation 

const cashButton4 = document.querySelector(".cashButton4");
const investmentButton4 = document.querySelector(".investButton4");
const realEstateButton4 = document.querySelector(".realEstateButton4");
const insuranceButton4 = document.querySelector(".insuranceButton4");
const legalPlanningButton4 = document.querySelector(".legalPlanningButton4");
const taxPlaningButton4 = document.querySelector(".taxPlaningButton4");

changeCardNavigation(cashButton4, cashCard);
changeCardNavigation(investmentButton4, investCard);
changeCardNavigation(realEstateButton4, rEstateCard);
changeCardNavigation(insuranceButton4, insuranceCard);
changeCardNavigation(taxPlaningButton4, taxCard);

// Tax planning navigation 

const cashButton5 = document.querySelector(".cashButton5");
const investmentButton5 = document.querySelector(".investButton5");
const realEstateButton5 = document.querySelector(".realEstateButton5");
const insuranceButton5 = document.querySelector(".insuranceButton5");
const legalPlanningButton5 = document.querySelector(".legalPlanningButton5");
const taxPlaningButton5 = document.querySelector(".taxPlaningButton5");

changeCardNavigation(cashButton5, cashCard);
changeCardNavigation(investmentButton5, investCard);
changeCardNavigation(realEstateButton5, rEstateCard);
changeCardNavigation(insuranceButton5, insuranceCard);
changeCardNavigation(legalPlanningButton5, legalCard);